# Translate

Для добавления переводов необходима необходима указать путь к json файлы.
Переменные окружения:
```
RU_TRANSLATE_RESOURCE_PATH
KK_TRANSLATE_RESOURCE_PATH
```
Файлы должны быть в json формате и имеет незвание [код языка.json]
```
ru.json
kk.json
en.json
```

### Пример инициализации 
```
ru.json
{
  "privileges.remain_days_from_day": {
    "one": "остался {{.Days}} день",
    "few": "осталось {{.Days}} дня",
    "many": "осталось {{.Days}} дней"
    "other": "other {{.Days}} days"
  }
}
```

```
import (
	"fmt"
	i18n2 "gitlab.com/go-lang-tools/i18n"
	"gitlab.com/go-lang-tools/i18n/initializers/i18n"
)

func main() {
	//инициализация bundle
	//bundle это структура словаря
	bundle := i18n.Initialize()

	responder := i18n2.NewResponder(bundle)

	language := "ru"

	localize := i18n2.NewLocalize(responder.I18nBundle, language)
	
	fmt.Println(localize.Tf("privileges.remain_days_from_day", map[string]interface{}{"Days": "1"}, 1))
}

остался 1 день
```
Если не указывать подставляющий значения ```templateData```  and  ```pluralCount``` то возьмет значения default из other

```
	fmt.Println(localize.Tf("privileges.remain_days_from_day", nil, 0))
	
other <no value> days	
```


ссылка на основную либу go-i18n: https://github.com/nicksnyder/go-i18n