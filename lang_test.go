package i18n

import (
	"context"
	"testing"
)

func TestLang_GetLangFromContext(t *testing.T) {
	type fields struct {
		defaultLang        string
		availableLanguages []string
		localizes          map[string]*Localize
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "case 1: удачный кейс",
			fields: fields{
				defaultLang:        "ru",
				availableLanguages: []string{"ru", "kk"},
				localizes:          map[string]*Localize{},
			},
			args: args{
				ctx: context.WithValue(context.Background(), langKey{}, "kk"),
			},
			want: "kk",
		},
		{
			name: "case 2: дэфолт ланг",
			fields: fields{
				defaultLang:        "ru",
				availableLanguages: []string{"ru", "kk"},
				localizes:          map[string]*Localize{},
			},
			args: args{
				ctx: context.WithValue(context.Background(), langKey{}, "en"),
			},
			want: "ru",
		},
		{
			name: "case 3: дэфолт ланг with test key",
			fields: fields{
				defaultLang:        "ru",
				availableLanguages: []string{"ru", "kk"},
				localizes:          map[string]*Localize{},
			},
			args: args{
				ctx: context.WithValue(context.Background(), struct{}{}, "en"),
			},
			want: "ru",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Lang{
				defaultLang:        tt.fields.defaultLang,
				availableLanguages: tt.fields.availableLanguages,
				localizes:          tt.fields.localizes,
			}
			if got := s.GetLangFromContext(tt.args.ctx); got != tt.want {
				t.Errorf("GetLangFromContext() = %v, want %v", got, tt.want)
			}
		})
	}
}
