package i18n

import (
	"encoding/json"

	"github.com/gobuffalo/envy"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/rs/zerolog/log"
	"golang.org/x/text/language"
)

const (
	ruTranslateResourceEnv = "RU_TRANSLATE_RESOURCE_PATH"
	kkTranslateResourceEnv = "KK_TRANSLATE_RESOURCE_PATH"
)

// Initialize initialize i18n bundle
func Initialize() *i18n.Bundle {
	ruPath := envy.Get(ruTranslateResourceEnv, "")
	kkPath := envy.Get(kkTranslateResourceEnv, "")

	paths := make([]string, 2)
	paths[0] = ruPath
	paths[1] = kkPath

	bundle := i18n.NewBundle(language.Russian)

	bundle.RegisterUnmarshalFunc("json", json.Unmarshal)

	for _, path := range paths {
		if _, errLoad := bundle.LoadMessageFile(path); errLoad != nil {
			log.Err(errLoad).Msgf("не удалось загрузить переводы из файла %s", path)
		}
	}

	return bundle
}
