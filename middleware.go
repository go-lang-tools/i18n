package i18n

import (
	"github.com/gin-gonic/gin"
)

const (
	httpHeaderLanguageKey       = "X-Language"
	httpHeaderAcceptLanguageKey = "Accept-Language"
)

// Middleware сохраняет указанный язык в Context
type Middleware struct {
	settings Settings
}

// NewMiddleware создает Middleware
func NewMiddleware(settings Settings) *Middleware {
	return &Middleware{settings: settings}
}

// SetLanguage вытаскивает из запроса язык и сохраняет его в Context
func (m *Middleware) SetLanguage(ctx *gin.Context) {
	lang := ctx.GetHeader(httpHeaderLanguageKey)
	if lang == "" {
		lang = ctx.GetHeader(httpHeaderAcceptLanguageKey)
	}

	if !m.settings.IsAvailableLang(lang) {
		lang = m.settings.GetDefaultLang()
	}

	ctx.Request = ctx.Request.WithContext(m.settings.SetLangToContext(ctx.Request.Context(), lang))
}
