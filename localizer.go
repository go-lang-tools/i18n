package i18n

import (
	base "github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/rs/zerolog/log"
)

const defaultLanguage = "ru"

// Localize локализатор
type Localize struct {
	base        *base.Localizer
	defaultBase *base.Localizer
}

// NewLocalize создает локализатор
func NewLocalize(bundle *base.Bundle, langs ...string) *Localize {
	return &Localize{
		base:        base.NewLocalizer(bundle, langs...),
		defaultBase: base.NewLocalizer(bundle, defaultLanguage),
	}
}

// T перевод
func (l *Localize) T(message string) string {
	localizeConfig := base.LocalizeConfig{
		MessageID: message,
	}

	translate, errLoc := l.base.Localize(&localizeConfig)
	if errLoc == nil {
		return translate
	}

	log.Err(errLoc).Msg("не удалось перевести")

	translate, _ = l.defaultBase.Localize(&localizeConfig)
	return translate
}

// Tf перевод с подстановкой данных
func (l *Localize) Tf(message string, templateData map[string]interface{}, pluralCount interface{}) string {
	localizeConfig := base.LocalizeConfig{
		MessageID:    message,
		TemplateData: templateData,
		PluralCount:  pluralCount,
	}

	translate, errLoc := l.base.Localize(&localizeConfig)
	if errLoc == nil {
		return translate
	}

	log.Err(errLoc).Msg("не удалось перевести")

	translate, _ = l.defaultBase.Localize(&localizeConfig)
	return translate
}
