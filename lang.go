package i18n

import (
	"context"

	"github.com/nicksnyder/go-i18n/v2/i18n"
)

// Settings настройки по языку
type Settings interface {
	GetDefaultLang() string
	IsAvailableLang(lang string) bool
	SetLangToContext(ctx context.Context, lang string) context.Context
}

const (
	defaultLang = "ru"
	kkLang      = "kk"
)

type langKey struct{}

var defaultAvailableLanguages = []string{defaultLang, kkLang}

// Lang утилита для получения языка из Context
type Lang struct {
	defaultLang        string
	availableLanguages []string
	localizes          map[string]*Localize
}

// NewLang создает язык по настройкам
func NewLang(defaultLang string, availableLanguages []string, bundle *i18n.Bundle) *Lang {
	localizes := map[string]*Localize{}

	for _, l := range availableLanguages {
		localizes[l] = NewLocalize(bundle, l)
	}

	return &Lang{
		defaultLang:        defaultLang,
		availableLanguages: availableLanguages,
		localizes:          localizes,
	}
}

// NewDefault создает язык по умолчанию
func NewDefault(bundle *i18n.Bundle) *Lang {
	return NewLang(defaultLang, defaultAvailableLanguages, bundle)
}

// GetLocalize получить локализатор по языку
func (s *Lang) GetLocalize(ctx context.Context) *Localize {
	lang := s.GetLangFromContext(ctx)

	if l, ok := s.localizes[lang]; ok {
		return l
	}

	return nil
}

// GetDefaultLang получить язык по умолчанию
func (s *Lang) GetDefaultLang() string {
	return s.defaultLang
}

// GetLangFromContext получить из Context язык, если нету вернуть язык по умолчанию
func (s *Lang) GetLangFromContext(ctx context.Context) string {
	if lang, ok := ctx.Value(langKey{}).(string); ok {
		if s.IsAvailableLang(lang) {
			return lang
		}
	}

	return s.GetDefaultLang()
}

// SetLangToContext сохранить язык в Context
func (s *Lang) SetLangToContext(ctx context.Context, lang string) context.Context {
	if ok := s.IsAvailableLang(lang); !ok {
		return ctx
	}

	return context.WithValue(ctx, langKey{}, lang)
}

// IsAvailableLang проверяет поддержку переданного языка
// Если передали пустую строку возвращает false
func (s *Lang) IsAvailableLang(lang string) bool {
	if lang == "" {
		return false
	}

	for _, l := range s.availableLanguages {
		if l == lang {
			return true
		}
	}

	return false
}
