package i18n

import bi18n "github.com/nicksnyder/go-i18n/v2/i18n"

// Responder ответчик
type Responder struct {
	I18nBundle *bi18n.Bundle
}

// NewResponder создает ответчик
func NewResponder(i18nBundle *bi18n.Bundle) *Responder {
	return &Responder{I18nBundle: i18nBundle}
}
